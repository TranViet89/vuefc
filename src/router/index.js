import Vue from 'vue'
import Router from 'vue-router'
import AddMistake from '@/components/AddMistake'
import ListMistakes from '@/components/ListMistakes'
import Mistake from '@/components/Mistake'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'AddMistake',
      component: AddMistake
    },
    {
      path: '/list-mistakes',
      name: 'ListMistakes',
      component: ListMistakes
    },
    {
      path: '/mistake/:id',
      name: 'Mistake',
      component: Mistake
    }
  ]
})
